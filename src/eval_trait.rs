use std::fmt::{Debug};
use std::ops::{Add};
use std::rc::{Rc};
use std::boxed::Box;

trait Eval {
    type Output;

    fn eval(&self) -> Self::Output;
}

struct Expr2<T> {
    nodes : Vec<Rc<dyn Eval<Output = T>>>,
}


impl Expr2 {
    fn new<T : Eval>(self : &Self, value : T) -> Constant<T> {
        self.
    }
}

struct Constant<T> {
    expr : Rc<Expr2>,
    index : usize
}


impl<T> Eval for Constant<T>
where T: Debug
{
    type Output = T;

    fn eval(&self) -> T {
        // will likely have to check the count
        // Rc::try_unwrap(self.value).unwrap()
        self.expr[self.index].
    }
}

struct EvalAdd<T>
{
    left: Rc<dyn Eval<Output = T>>,
    right: Rc<dyn Eval<Output = T>>,
}

impl<T> Eval for EvalAdd<T>
where T : Eval<Output = T> + Debug
{
    type Output = T;

    fn eval(&self) -> Self::Output {
        self.left.eval()
    }
}

impl<T> Add for &dyn Eval<Output = T>
{
    type Output = EvalAdd<T>;

    fn add(self : Self, rhs : Self) -> Self::Output {
        EvalAdd { left : Rc::new(self), right : Rc::new(rhs) }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_binary_plus() {
        let c1 = Constant {value: 1.2};
        let c2 = Constant {value: 3.4};

        println!("c1 = {:?}", c1);  // output: c1 = Constant { value: 1.2 }
        println!("c2 = {:?}", c2);  // output: c2 = Constant { value: 3.4 }

    
        let bp1 = BinaryPlus { left: &c1, right: &c2 };
        println!("bp1 = {:?}", bp1);              // output: bp1 = BinaryPlus { left: Constant { value: 1.2 }, right: Constant { value: 3.4 } }
        println!("bp1.eval() = {}", bp1.eval());  // output: bp1.eval() = 4.6
    
        // ownership check: make sure bp1 doesn't now own c1 and c2
        println!("c1.eval() = {}", c1.eval());    // output: c1.eval() = 1.2
        println!("c2.eval() = {}", c2.eval());    // output: c2.eval() = 3.4
    
        let bp2 = BinaryPlus { left: &c1, right: &bp1 };
        println!("bp2 = {:?}", bp2);              // output: bp2 = BinaryPlus { left: Constant { value: 1.2 }, right: BinaryPlus { left: Constant { value: 1.2 }, right: Constant { value: 3.4 } } }
        println!("bp2.eval() = {}", bp2.eval()); 
    }

    fn test_add() {
        let c1 = Constant {value: 1.2};
        let c2 = Constant {value: 3.4};

        let d = c1 + c2;
    }
}
