# Expr-tree

An expression tree parser and evaluator framework.

Focus is on being generic and not on performance, though it should be 
fast enough.

`expr-tree` is mostly a crate to reduce boilerplate code for creating 
computational trees of Rust expresisons that you can evaluate for any
result.

This crate was created specifically for `expr-autodiff`, a reverse mode
auto differentiator, to be used in a future neural network library.

## Usage

Using `expr_tree` starts with creating an `Expr` object. This object
creates the variables to be used in the expression, and then the
expression itself.

A simple example:

```
let e = Expr::<f64>::new();

let a = e.var(1.0);
let b = e.var(2.0);
let c = a + b;

assert_eq!(c.eval().unwrap(), 3.0);
```

The `Expr` struct holds the variables and returns references
when new variables are created with `var`.

Calling `eval` on the `Expr` returns the value for the entire
expression.[^1]. 

[^1]: Not yet implemented, but the eventual
code will allow for reassigning new values to variables
and then re-evaluations will evaluate with the new values.

Calling `eval` on any node returns the evaluation of the
expression from that node down below. 

## Custom Functions and Types

TBD = To Be Described

## TO DO

- [X] Compile time type-safe evaluation
- [X] Break into modules
- [ ] Variable re-evaluation
- [ ] Add function support
- [ ] Include library of common functions (sin, cos, sigmoid, relu, etc.)
- [ ] Operator/Function impl macros to reduce boilerplate
- [ ] Upload to crates.io
- [ ] Support for common libraries
    - [ ] Num
    - [ ] Vec
    - [ ] ndarray
    - [ ] Polars?
- [ ] Proc macros for syntax sugar
- [ ] Autodiff example use

