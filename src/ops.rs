use std::ops::{Add, Sub, Mul, Div};

use crate::expr::{Expr};
use super::*;

pub struct ExprAdd<'a, T> {
    pub expr: &'a Expr<'a, T>,
    pub left: &'a dyn Eval<'a, T, Output = &'a T>,
    pub right: &'a dyn Eval<'a, T, Output = &'a T>,
}

impl<'a> Eval<'a, f64> for ExprAdd<'a, f64> {
    type Output = &'a f64;

    fn eval(&'a self) -> Self::Output {
        self.expr.value(self.left.eval() + self.right.eval())
    }

    fn expr(&'a self) -> &Expr<f64> {
        self.expr
    }
}

impl<'a> Add<&'a dyn Eval<'a, f64, Output = &'a f64>> for &'a dyn Eval<'a, f64, Output = &'a f64> {
    type Output = &'a dyn Eval<'a, f64, Output = &'a f64>;

    fn add(self, other: Self) -> Self::Output {
        self.expr().op(ExprAdd {
            expr: self.expr(),
            left: self,
            right: other,
        })
    }
}

impl<'a> Eval<'a, Vec<f64>> for ExprAdd<'a, Vec<f64>> {
    type Output = &'a Vec<f64>;

    fn eval(&'a self) -> Self::Output {
        self.expr.value(
            self.left
                .eval()
                .iter()
                .zip(self.right.eval().iter())
                .map(|(x, y)| x + y)
                .collect(),
        )
    }

    fn expr(&'a self) -> &Expr<Vec<f64>> {
        self.expr
    }
}

impl<'a> Add<&'a dyn Eval<'a, Vec<f64>, Output = &'a Vec<f64>>>
    for &'a dyn Eval<'a, Vec<f64>, Output = &'a Vec<f64>>
{
    type Output = &'a dyn Eval<'a, Vec<f64>, Output = &'a Vec<f64>>;

    fn add(self, other: Self) -> Self::Output {
        self.expr().op(ExprAdd {
            expr: self.expr(),
            left: self,
            right: other,
        })
    }
}

pub struct ExprSub<'a, T> {
    pub expr: &'a Expr<'a, T>,
    pub left: &'a dyn Eval<'a, T, Output = &'a T>,
    pub right: &'a dyn Eval<'a, T, Output = &'a T>,
}

impl<'a> Eval<'a, f64> for ExprSub<'a, f64> {
    type Output = &'a f64;

    fn eval(&'a self) -> Self::Output {
        self.expr.value(self.left.eval() - self.right.eval())
    }

    fn expr(&'a self) -> &Expr<f64> {
        self.expr
    }
}

impl<'a> Sub<&'a dyn Eval<'a, f64, Output = &'a f64>> for &'a dyn Eval<'a, f64, Output = &'a f64> {
    type Output = &'a dyn Eval<'a, f64, Output = &'a f64>;

    fn sub(self, other: Self) -> Self::Output {
        self.expr().op(ExprSub {
            expr: self.expr(),
            left: self,
            right: other,
        })
    }
}

impl<'a> Eval<'a, Vec<f64>> for ExprSub<'a, Vec<f64>> {
    type Output = &'a Vec<f64>;

    fn eval(&'a self) -> Self::Output {
        self.expr.value(
            self.left
                .eval()
                .iter()
                .zip(self.right.eval().iter())
                .map(|(x, y)| x + y)
                .collect(),
        )
    }

    fn expr(&'a self) -> &Expr<Vec<f64>> {
        self.expr
    }
}

impl<'a> Sub<&'a dyn Eval<'a, Vec<f64>, Output = &'a Vec<f64>>>
    for &'a dyn Eval<'a, Vec<f64>, Output = &'a Vec<f64>>
{
    type Output = &'a dyn Eval<'a, Vec<f64>, Output = &'a Vec<f64>>;

    fn sub(self, other: Self) -> Self::Output {
        self.expr().op(ExprSub {
            expr: self.expr(),
            left: self,
            right: other,
        })
    }
}
