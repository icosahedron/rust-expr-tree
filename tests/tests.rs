// use std::f64::consts::PI;
// use std::fmt::Debug;
// use std::collections::{HashMap};
// use std::rc::{Rc};

use expr_tree::prelude::*;

#[test]
fn test_f64_add_tree() {
    let e = Expr::new();
    let a = e.var(1.0);
    let b = e.var(2.0);
    let d = a + b + a;

    // println!("{:?}", d);
}

#[test]
fn test_f64_add_node_eval() {
    // simple f64 expression to add
    let e = Expr::new();
    let a = e.var(1.0);
    let b = e.var(2.0);
    let d = a + b + a - b;

    assert_eq!(*e.eval(), 2.0);
}

#[test]
fn test_var_f64_add_eval() {    // simple Vec<f64> expression to add (requires a specific add since Add isn't defined for Vec)
    let e = Expr::<Vec<f64>>::new();
    let i = e.var(vec![1.0, 2.0, 3.0]);
    let j = e.var(vec![3.0, 2.0, 1.0]);
    let l = i + j;

    assert_eq!(e.eval(), &vec![4.0, 4.0, 4.0]);
}

#[test]
fn test_f64_add_expr_eval() {
    // simple f64 expression to add
    let e = Expr::new();
    let a = e.var(1.0);
    let b = e.var(2.0);
    let d = a + b + a - b;

    assert_eq!(*e.eval(), 2.0);
}



// #[test]
// fn test_expr_add_f64_tree() {
//     let a = Expr::new(1.0);
//     let b = Expr::new(2.0);

//     let c = &a + &b;
//     assert_eq!(a.value().unwrap(), &1.0);
//     assert_eq!(b.value().unwrap(), &2.0);
//     assert_eq!(c.is_value(), false);
//     match c {
//         Expr::Op(op, _params) => assert_eq!(op, "add"),
//         _ => assert_eq!(1, 0)
//     }
// }

// #[test]
// fn test_expr_add_vec_f64_tree() {
//     let a = Expr::new(vec![1.0, 2.0, 3.0]);
//     let b = Expr::new(vec![4.0, 5.0, 6.0]);

//     let c = &a + &b;
//     assert_eq!(a.value().unwrap(), &vec![1.0, 2.0, 3.0]);
//     assert_eq!(b.value().unwrap(), &vec![4.0, 5.0, 6.0]);
//     assert_eq!(c.is_value(), false);
// }

// fn sample<T>(p1 : &Expr<T>, p2 : &Expr<T>) -> Expr<T> {
//     Expr::<T>::Op("sample", Rc::new(vec![p1.clone(), p2.clone()]))
// }

// #[test]
// fn test_expr_add_vec_f64_custom_function_tree() {
//     let a = &Expr::new(vec![1.0, 2.0, 3.0]);
//     let b = &Expr::new(vec![4.0, 5.0, 6.0]);

//     let c = a + b + sample(a, b);

//     assert_eq!(a.value().unwrap(), &vec![1.0, 2.0, 3.0]);
//     assert_eq!(b.value().unwrap(), &vec![4.0, 5.0, 6.0]);
//     assert_eq!(c.is_value(), false);
//     match c {
//         Expr::Op(op, _params) => assert_eq!(op, "add"),
//         _ => assert_eq!(1, 0, "Expected 'add' as operator")
//     }
// }

// #[test]
// fn test_expr_mul_vec_f64_custom_function_tree() {
//     let a = &Expr::new(vec![1.0, 2.0, 3.0]);
//     let b = &Expr::new(vec![4.0, 5.0, 6.0]);

//     let c = a * b * sample(a, b);

//     assert_eq!(a.value().unwrap(), &vec![1.0, 2.0, 3.0]);
//     assert_eq!(b.value().unwrap(), &vec![4.0, 5.0, 6.0]);
//     assert_eq!(c.is_value(), false);
//     match c {
//         Expr::Op(op, _params) => assert_eq!(op, "mul"),
//         _ => assert_eq!(1, 0, "Expected 'mul' as operator")
//     }
// }

// #[test]
// fn test_expr_f64_eval() {
//     let a = Expr::new(1.0);
//     let e = ExprEval::new(HashMap::new());
//     let c = e.eval(a);
//     assert_eq!(c.unwrap(), 1.0);
// }

// #[test]
// fn test_expr_vec_f64_eval() {
//     let a = Expr::new(vec![1.0, 2.0, 3.0]);
//     let e = ExprEval::new(HashMap::new());

//     let c = e.eval(a);
//     assert_eq!(c.unwrap(), vec![1.0, 2.0, 3.0]);
// }

// #[test]
// fn test_expr_f64_add_eval() {
//     let ops: HashMap<&str, ExprOp<f64>> = HashMap::from([
//         ("add", |p : &[&f64]| -> f64 { p[0] + p[1] } as ExprOp<f64>),
//     ]);
//     let a = Expr::new(1.0);
//     let b = Expr::new(2.0);
//     let c = a + b;
//     let e = ExprEval::new(ops);

//     let c = e.eval(c);
//     assert_eq!(c.unwrap(), 3.0);
// }

// fn op_add_vec_f64(p : &[&Vec<f64>]) -> Vec<f64> {
//     let a = p[0];
//     let b = p[1];
//     let vec_add = a.iter().zip(b.iter()).map(|(a, b)| *a + *b).collect();
//     vec_add
// }

// #[test]
// fn test_expr_vec_f64_add_eval() {
//     let ops: HashMap<&str, ExprOp<Vec<f64>>> = HashMap::from([
//         ("add", op_add_vec_f64 as ExprOp<Vec<f64>>),
//     ]);
//     let a = Expr::new(vec![1.0, 2.0, 3.0]);
//     let b = Expr::new(vec![-1.0, -2.0, -3.0]);
//     let c = a + b;
//     let e = ExprEval::new(ops);
//     let result = e.eval(c);

//     assert_eq!(result.unwrap(), vec![0., 0., 0.]);
// }

// #[test]
// fn test_expr_vec_f64_mul_eval() {
//     let ops: HashMap<&str, ExprOp<Vec<f64>>> = HashMap::from([
//         ("mul", |p : &[&Vec<f64>]| -> Vec<f64> { 
//             p[0].iter().zip(p[1].iter()).map(|(a, b)| *a * *b).collect() 
//         } as ExprOp<Vec<f64>>),
//     ]);
//     let a = Expr::new(vec![1.0, 2.0, 3.0]);
//     let b = Expr::new(vec![2.0, 2.0, 2.0]);
//     let c = a * b;
//     let e = ExprEval::new(ops);

//     let result = e.eval(c);
//     assert_eq!(result.unwrap(), vec![2., 4., 6.]);
// }

// fn sin<T>(p1 : &Expr<T>) -> Expr<T> {
//     Expr::<T>::Op("sin", Rc::new(vec![p1.clone()]))
// }

// #[test]
// fn test_expr_f64_sin_eval() {
//     let ops: HashMap<&str, ExprOp<f64>> = HashMap::from([
//         ("sin", |p : &[&f64]| -> f64 { p[0].sin() } as ExprOp<f64>),
//         ("mul", |p : &[&f64]| -> f64 { p[0] * p[1] } as ExprOp<f64>),
//     ]);
//     let a = Expr::new(PI);
//     let b = Expr::new(0.5);
//     let c = sin(&(a * b));
//     let e = ExprEval::new(ops);

//     let c = e.eval(c);
//     assert_eq!(c.unwrap(), 1.0);
// }

// #[derive(PartialEq, Debug)]
// enum VecScalar {
//     Vec(Vec<f64>),
//     Scalar(f64),
// }

// #[test]
// fn test_expr_vec_by_scalar_f64() {
//     let ops: HashMap<&str, ExprOp<VecScalar>> = HashMap::from([
//         ("mul", |p : &[&VecScalar]| -> VecScalar { 
//             if let VecScalar::Vec(v) = p[0] {
//                 if let VecScalar::Scalar(s) = p[1] {
//                     VecScalar::Vec(v.iter().map(|e| e * s).collect())
//                 }
//                 else {
//                     VecScalar::Scalar(-1.0)
//                 }
//             }
//             else {
//                 VecScalar::Scalar(-1.0)
//             }
//         } as ExprOp<VecScalar>),
//     ]);

//     let a : Expr<VecScalar> = Expr::new(VecScalar::Vec(vec![1.0, 2.0, 3.0]));
//     let b = Expr::new(VecScalar::Scalar(2.0));

//     let c = a * b;
//     let e = ExprEval::new(ops);

//     let result = e.eval(c);
//     assert_eq!(result.unwrap(), VecScalar::Vec(vec![2., 4., 6.]));
// }

// #[test]
// fn test_ops_same_variable_f64() {
//     let ops: HashMap<&str, ExprOp<f64>> = HashMap::from([
//         ("add", |p : &[&f64]| -> f64 { p[0] + p[1] } as ExprOp<f64>),
//         ("mul", |p : &[&f64]| -> f64 { p[0] * p[1] } as ExprOp<f64>),
//     ]);
//     let e = ExprEval::new(ops);

//     let a = Expr::new(2.0);

//     let v = &a + &a * &a;
//     let r = e.eval(v);

//     assert_eq!(r.unwrap(), 6.0);
// }

// #[test]
// fn test_ops_same_variable_vec_f64() {
//     let ops: HashMap<&str, ExprOp<Vec<f64>>> = HashMap::from([
//         ("mul", |p : &[&Vec<f64>]| -> Vec<f64> { 
//             p[0].iter().zip(p[1].iter()).map(|(a, b)| *a * *b).collect() 
//         } as ExprOp<Vec<f64>>),
//         ("add", |p : &[&Vec<f64>]| -> Vec<f64> { 
//             p[0].iter().zip(p[1].iter()).map(|(a, b)| *a + *b).collect() 
//         } as ExprOp<Vec<f64>>),
//     ]);


//     let a = Expr::new(vec![2.0, 2.0, 2.0]);

//     let v = &a + &a * &a;
//     let e = ExprEval::new(ops);

//     let r = e.eval(v);
//     assert_eq!(r.unwrap(), vec![6.0, 6.0, 6.0]);
// }

// // This is an experiment to see if we could create a type-safe, generic way
// // to provide implemented operators and functions to users. They would still
// // have to implement the eval function for their operators, but this boilerplate
// // would be done for them.
// // I guess SFINAE is not implemented for Rust when it comes to enums or associated
// // values. 
// #[cfg(experiment)]
// mod failed_experiment { 

// use anyhow::*;
// use std::marker::{Copy};
// use std::ops::{Add};

// #[derive(Debug, Clone, Copy)]
// enum TestOp {
//     Add,
//     // Sub,
//     // Mul,
//     // Div
// }

// #[derive(Debug)]
// pub enum TestExpr<T, Ops> 
// where Ops : Copy 
// {
//     Value(Rc<T>),
//     Op(Ops, Rc<Vec<TestExpr<T, Ops>>>),
//     Error(String),
// }

// type TestOpFn<T> = fn (&[&T]) -> T;

// trait TestEval {
//     type Output;

//     fn eval(self : Self) -> Result<Self::Output>;
// }

// type TestEx<T> = TestExpr<T, TestOp>;

// impl<T, Ops> TestExpr<T, Ops> 
// where Ops : Copy
// {
//     pub fn new(v : T) -> TestExpr<T, Ops> {
//         TestExpr::Value(Rc::new(v))
//     }

//     pub fn is_value(self : &Self) -> bool {
//         match self {
//             TestExpr::Value(_) => true,
//             _ => false
//         }
//     }

//     pub fn value(self : &Self) -> Result<&T> {
//         match self {
//             TestExpr::Value(v) => Ok(v.as_ref()),
//             _ => Err(anyhow!("Operations do not have values."))
//         }
//     }

//     pub fn value_clone(self : &Self) -> Result<Rc<T>> {
//         match self {
//             TestExpr::Value(v) => Ok(v.clone()),
//             _ => Err(anyhow!("Operations do not have values."))
//         }
//     }

//     pub fn is_op(self : &Self) -> bool {
//         match self {
//             TestExpr::Op(_, _) => true,
//             _ => false
//         }
//     }

//     pub fn op(self : &Self) -> Result<(Ops, Rc<Vec<TestExpr<T, Ops>>>)> {
//         match self {
//             TestExpr::Op(op, params) => Ok((*op, params.clone())),
//             _ => Err(anyhow!("Values do not have operations."))
//         }
//     }
// }

// impl<T, Ops> Clone for TestExpr<T, Ops>
// where Ops : Copy
// {
//     fn clone(&self) -> Self {
//         match self {
//             TestExpr::Value(v) => TestExpr::Value(v.clone()),// Expr::ValueRef(Rc::downgrade(v)),
//             // Expr::ValueRef(w) => Expr::ValueRef(w.clone()),
//             TestExpr::Op(op, params) => TestExpr::Op(*op, params.clone()),
//             TestExpr::Error(e) => TestExpr::Error(e.to_string())
//         }
//     }

//     fn clone_from(&mut self, _source: &Self) {
//         panic!("Expr::clone_from not implemented.");
//     }
// }

// impl TestEval for TestEx<f64> 
// {
//     type Output = f64;

//     fn eval(self : Self) -> Result<f64> {
//         match self {
//             TestExpr::Value(v) => Ok(Rc::try_unwrap(v).unwrap()),
//             TestExpr::Op(_, _) => {
//                 match eval_tree(&self) {
//                     TestExpr::Value(v) => Ok(Rc::try_unwrap(v).unwrap()),
//                     TestExpr::Op(_, _) => Err(anyhow!("Evaluation ended on an operation. It should always end on a value.")),
//                     TestExpr::Error(e) => Err(anyhow!(e)),
//                 }
//             },
//             _ => Err(anyhow!("Root expression nodes must be either a value or an operation."))
//         }
//     }
// }

// fn eval_tree(expr : &TestExpr<f64, TestOp>) -> TestExpr<f64, TestOp> {
//     match expr {
//         TestExpr::Value(_) => expr.clone(),
//         TestExpr::Op(op, params) => {
//             let values : Vec<TestExpr<f64, TestOp>> = params.iter().map(|p| eval_tree(p)).collect();
//             // let value_refs : Vec<&f64> = values.iter().map(|v| v.value().unwrap()).collect();
//             match op {
//                 TestOp::Add => TestExpr::Value(Rc::new(values[0].value().unwrap() + values[1].value().unwrap()))
//             }
//         },
//         TestExpr::Error(e) => TestExpr::Error(format!("eval_tree: {}", e))
//     }
// }

// impl<T, Ops> Add<&TestExpr<T, Ops>> for &TestExpr<T, Ops>
// where Ops : Copy
// {
//     type Output = TestExpr<T, Ops>;

//     fn add(self : Self, rhs : &TestExpr<T, Ops>) -> Self::Output {
//         TestExpr::Op(Ops::Add, Rc::new(vec![self.clone(), rhs.clone()]) )
//     }
// }

// impl<T, Ops> Add<TestExpr<T, Ops>> for &TestExpr<T, Ops> 
// where Ops : Copy
// {
//     type Output = TestExpr<T, Ops>;

//     fn add(self : Self, rhs : TestExpr<T, Ops>) -> Self::Output {
//         TestExpr::Op(Ops::Add, Rc::new(vec![self.clone(), rhs.clone()]) )
//     }
// }

// impl<T, Ops> Add<TestExpr<T, Ops>> for TestExpr<T, Ops> 
// where Ops : Copy
// {
//     type Output = Self;

//     fn add(self : Self, rhs : Self) -> Self::Output {
//         TestExpr::Op(Ops::Add, Rc::new(vec![self.clone(), rhs.clone()]) )
//     }
// }


// #[test]
// fn test_testexpr() {
//     let a = TestExpr::new(1.0);

//     assert_eq!(a.eval().unwrap(), 1.0);
// }

// #[test]
// fn test_testexpr_add() {
//     let a = TestExpr::new(2.0);
//     let b = TestExpr::new(3.0);

//     let c = a + b;

//     let d = c.eval();

//     assert_eq!(d.eval().unwrap(), 5.0);
// }

// // impl<T, const Op : usize> TestExpr<T, New> {
// //     pub fn new(v : T) -> TestExpr<T, New> {
// //         TestExpr::Value(Rc::new(v))
// //     }

// //     pub fn is_value(self : &Self) -> bool {
// //         match self {
// //             TestExpr::Value(_) => true,
// //             _ => false
// //         }
// //     }

// //     pub fn value(self : &Self) -> Result<&T> {
// //         match self {
// //             TestExpr::Value(v) => Ok(v.as_ref()),
// //             // Expr::ValueRef(w) => {
// //             //     let t = unsafe { w.as_ptr().as_ref() }.unwrap();
// //             //     Ok(t)
// //             // }
// //             _ => Err(anyhow!("Operations do not have values."))
// //         }
// //     }

// //     pub fn value_clone(self : &Self) -> Result<Rc<T>> {
// //         match self {
// //             TestExpr::Value(v) => Ok(v.clone()),
// //             // Expr::ValueRef(w) => Ok(w.upgrade().unwrap().clone()),
// //             _ => Err(anyhow!("Operations do not have values."))
// //         }
// //     }

// //     pub fn is_op(self : &Self) -> bool {
// //         match self {
// //             TestExpr::Op(_, _) => true,
// //             _ => false
// //         }
// //     }

// //     pub fn op(self : &Self) -> Result<(usize, Rc<Vec<TestExpr<T>>>)> {
// //         match self {
// //             TestExpr::Op(op, params) => Ok((*op, params.clone())),
// //             _ => Err(anyhow!("Values do not have operations."))
// //         }
// //     }
// // }

// // #[repr(usize)]
// // enum TestOps {
// //     Sum = 1,
// //     Sub,
// // }

// // use std::ops::{Add};

// // mod TestOps {
// //     pub const New : usize = 0;
// //     pub const Add : usize = 1;
// //     pub const Sub : usize = 2;
// // }

// // struct TestEval<const Op: usize = 0>;

// // // trait TestEvalOp <T>
// // // where T : Default {
// // //     fn eval(self : &Self, expr : TestExpr<T>) -> Result<T>;
// // // }

// // impl TestEval<{ TestOps::New }> {
// //     fn new() -> Self {
// //         TestEval { }
// //     }
// // }

// // impl TestEval<{ TestOps::Add }> 
// // {
// //     fn eval<T : Default>(self : &Self, _expr : TestExpr<T>) -> Result<T> {
// //         Ok(T::default())
// //     }
// // }

// // impl<T> Add<&TestExpr<T>> for &TestExpr<T>
// // {
// //     type Output = TestExpr<T>;

// //     fn add(self : Self, rhs : &TestExpr<T>) -> Self::Output {
// //         TestExpr::Op(TestOps::Add, Rc::new(vec![self.clone(), rhs.clone()]) )
// //     }
// // }

// // impl<T> Add<TestExpr<T>> for &TestExpr<T> {
// //     type Output = TestExpr<T>;

// //     fn add(self : Self, rhs : TestExpr<T>) -> Self::Output {
// //         TestExpr::Op(TestOps::Add, Rc::new(vec![self.clone(), rhs.clone()]) )
// //     }
// // }

// // impl<'a, T> Add<TestExpr<T>> for TestExpr<T> {
// //     type Output = TestExpr<T>;

// //     fn add(self : Self, rhs : Self) -> Self::Output {
// //         TestExpr::Op(TestOps::Add, Rc::new(vec![self.clone(), rhs.clone()]) )
// //     }
// // }

// // #[test]
// // fn test_generic_const() {
// //     let a = TestExpr::new(PI);
// //     let b = TestExpr::new(0.5);

// //     let c = a + b;

// //     let e = TestEval::new();
// // }
// }