
pub mod expr;
pub mod ops;

use expr::{Expr};

pub mod prelude {
    pub use super::expr::*;
    pub use super::ops::*;
}

pub trait Eval<'a, T> {
    type Output;

    fn eval(&'a self) -> Self::Output;
    fn expr(&'a self) -> &Expr<T>;
}


