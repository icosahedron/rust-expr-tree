use bumpalo::Bump;
use std::{cell::UnsafeCell, marker::PhantomData};

use super::Eval;

pub struct Expr<'a, T> {
    pub(crate) alloc: Bump,
    vars : UnsafeCell<Vec<T>>,
    last_op : UnsafeCell<Option<&'a dyn Eval<'a, T, Output = &'a T>>>,
}

pub struct Var<'a, T> {
    expr: &'a Expr<'a, T>,
    index : usize,
}

impl<'a, T> Expr<'a, T> {
    pub fn new() -> Self {
        Expr {
            alloc: Bump::new(),
            vars: UnsafeCell::new(Vec::new()),
            last_op : UnsafeCell::new(None),
        }
    }

    pub fn eval(&self) -> &T {
        unsafe { (*self.last_op.get()).unwrap().eval() }
    }

    pub fn var(&'a self, value: T) -> &'a dyn Eval<T, Output = &'a T>  {
        unsafe { 
            (*self.vars.get()).push(value);
            let index = (*self.vars.get()).len() - 1;
            &*self.alloc.alloc(Var { expr : self, index : index, /* phantom_lifetime : &(), phantom_type : PhantomData */ })
        }
    }

    pub fn op<E: Eval<'a, T, Output = &'a T> + 'a>(
        &'a self,
        op: E,
    ) -> &'a dyn Eval<T, Output = &'a T> {
        let op = &*self.alloc.alloc(op);
        unsafe { (*self.last_op.get()) = Some(op); }
        op
    }

    pub fn value(&'a self, value: T) -> &'a T {
        &*self.alloc.alloc(value)
    }
}

impl<'a, T> Eval<'a, T> for Var<'a, T> {
    type Output = &'a T;

    fn eval(&'a self) -> Self::Output {
        unsafe { &(*self.expr.vars.get())[self.index] }
    }

    fn expr(&'a self) -> &Expr<T> {
        self.expr
    }
}
